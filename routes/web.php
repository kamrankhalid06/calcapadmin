<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function (){
   return view('home');
});


Route::group(['middleware'=>'auth', 'namespace'=>'Admin', 'as'=>'admin.', 'prefix'=>'admin'], function (){

    Route::get('/dashboard', [
        'as' => 'dashboard',
        'uses' => 'Dashboard@index',
    ]);

    Route::resource('/careers', 'CareersController');
    Route::resource('/locations', 'LocationController');
    Route::resource('/mng_srvc', 'ManagementServicesController');
    Route::resource('locations', 'LocationController');
});


Auth::routes();