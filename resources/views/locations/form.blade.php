<div class='form-group'>
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('area', 'Area (Sq ft):') !!}
    {!! Form::text('area', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('bedrooms', 'Bedrooms:') !!}
    {!! Form::text('bedrooms', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('bathrooms', 'Bathrooms:') !!}
    {!! Form::text('bathrooms', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('link', 'Link:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div>
<div class='form-group'>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-lg btn-success form-control']) !!}
</div>