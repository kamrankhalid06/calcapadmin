@extends('layouts.app')
@section('content')
    <div class='col-md-6 col-md-offset-3'>
        <h1>Edit Candidate</h1>
        <hr>

        {!! Form::model($location, ['method' => 'PATCH', 'action' => ['Admin\LocationController@update',$location->id]]) !!}
        @include('locations.form', ['submitButtonText' => 'Edit Candidate'])
        {!! Form::close() !!}
    </div>
@stop