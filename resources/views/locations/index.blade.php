@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Area sq ft</th>
                            <th scope="col">No of beds</th>
                            <th scope="col">No of baths</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($locations as $location)
                            <tr>
                                <th scope="row">
                                    <a href="{{ url('/admin/locations', $location->id) }}">
                                        {{ $location->name }}
                                    </a>
                                </th>
                                <td>{{ $location->area }}</td>
                                <td>{{ $location->beds }}</td>
                                <td>{{ $location->baths }}</td>
                                <td>
                                    <a href="{{ route('admin.locations.edit', $location->id) }}">
                                        Edit Location
                                    </a>
                                    <form action="/task/{{ $task->id }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button>Delete Task</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                                <p>There are no location to display!</p>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection