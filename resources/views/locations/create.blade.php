@extends('layouts.app')
@section('content')
    <div class='col-md-6 col-md-offset-3'>
        <h1>Add New Candidate</h1>
        <hr>

        {!! Form::open(['action' => 'Admin\LocationController@store', 'id' => 'community_location', 'files' => true]) !!}
        @include('locations.form', ['submitButtonText' => 'Add Candidate'])
        {!! Form::close() !!}
    </div>
@stop