<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('name');
            $table->mediumText('address');
            $table->longText('description');
            $table->integer('area');
            $table->integer('bedrooms');
            $table->integer('bathrooms');
            $table->char('link', '250');
            $table->char('latitude', '250');
            $table->char('longitude', '250');
            $table->char('image', '250');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
