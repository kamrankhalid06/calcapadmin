<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [ 'name', 'address', 'description', 'area', 'bedrooms', 'bathrooms', 'link', 'latitude', 'longitude', 'image'];
}
