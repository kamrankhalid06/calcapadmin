<?php

namespace App\Http\Controllers\Admin;

use App\ManagementServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagementServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ManagementServices  $managementServices
     * @return \Illuminate\Http\Response
     */
    public function show(ManagementServices $managementServices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ManagementServices  $managementServices
     * @return \Illuminate\Http\Response
     */
    public function edit(ManagementServices $managementServices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ManagementServices  $managementServices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ManagementServices $managementServices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ManagementServices  $managementServices
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManagementServices $managementServices)
    {
        //
    }
}
