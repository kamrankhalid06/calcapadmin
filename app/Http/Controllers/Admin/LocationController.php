<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::all();
        return view('locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  $flight = new Flight;
        //  $flight->name = $request->name;
        //  $flight->save();
        $input = $request->all();
        Location::create($input);
        return redirect('admin/locations');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //  $locationRec = Location::findorFail($location);
        return view('locations.show', compact('location'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        return view('locations.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        // $locationRec = Location::findOrFail($location);
        $input = $request->all();
        $location->update($input);
        return redirect('admin/locations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $locationRec = Location::findOrFail($id);
        $locationRec->delete();

        return redirect('admin/locations');
    }
}
