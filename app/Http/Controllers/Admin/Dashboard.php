<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class Dashboard extends Controller
{

    /**
     * Dashboard constructor.
     */
    public function __construct()
    {
    }

    public function index(){
        return view('welcome');
    }

    public function showUsers(){
        return response()->json(User::get(), 200);
    }

    public function showUser($id){
        return response()->json(User::find($id), 200);
    }
}
